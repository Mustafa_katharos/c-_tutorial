//Rabbim ismiyle

#include <iostream>
#include <cstdio>
#include <cstdarg>
#include <format>
#include <string>
#include <fstream>
#include <istream>

using namespace std;


/* *-----FUNCTIONS-----*
   * 
    Note ⁿ: 
        Functions couldn't be declared after the main function
        whereas it could be declared before the main function
        and *defined after it...
   */

//■ Declaring a function (A)
void muFunc() {
    cout << "This Function has been executed successfully.";
}

//■ Function (B) declaration
void sigFunc();


//■ Function (C) Parameterised declaration.
void epsFunc(string name);

//■ Function (D) Parameterised decalaration with default argument **(with applying string formatting).
void upsFunc(int age = 62);

//■ Function (E) multiple parameterisation declaration **(with applying string formatting).
void piFunc(string name, string password);

        // *-----FUNCTION OVERLOADING-----*
/*
*   Note thatⁿ:
*           Function Overloading is implemented by changing the parameters type or count
*           for differing between both functions.
*/
int func(int x, int y) {
    int z = x + y;
    cout << "\nInteger value equals: " << z;
    return z;
}
double func(double x, double y) {
    double z = x + y;
    cout << "\nDouble value equals: " << z;
    return z;
}

// *------------------CLASSES------------------*
class TestClass {
public:
    int number;
    string name;

    // ■⌠ Class Constructor.
    TestClass() {
        cout << "\nThe Class Constructor has been implemented Successfully.";
    }

    // ■⌠ Parameterised Class Constructor.
    TestClass(string name) {
        printf("\nWelcome %s Efendim.", name.c_str());
    }

    // ■⌠ Inner Class Constructor declaration.
    TestClass(string name, string phone_num);

    // ■⌠ Inner Class Function Declaration and Implementation.
    int innerFuncTest(int x, int y){
        return x + y;
    }
    

    // ╚ Outer Class Function Declaration
    float outerFuncTest(float x, float y);


    //ⁿ╚ Public setter and getter
    void setPassword(string password){
        this->password = password;
    }

    string getPassword() {
        return password;
    }

    // ├ Application α) Read file 
    void readFile();

/* Private Specifier
*   Private •attributes | •Methods | •Constructors can not be accessed from outside the class...
*   ╚ for accessing or alterihng their values; __USE {setter and getter}....
*/
private:
    string password = "02134141";
};


int main()
{
    
    /*int x;

    cout << "Enter a number: ";

    cin >> x;


    // *-----SWITCH CASES-----*
    switch (x){
        case 1:                    
            cout << "one";            case 2:
            break;                      cout << "two";      case 3:
                                        break;                cout << "three";    case 4:
                                                              break;                cout << "four";
                                                                                    break;

        case 5:
            cout << "five";           case 6:
            break;                      cout << "six";      case 7:
                                        break;                cout << "seven";    case 8:
                                                              break;                cout << "eight";
                                                                                    break;
                                                            
        case 9:
            cout << "nine";           case 10:
            break;                      cout << "ten";      default:
                                        break;                cout << "Choose a number for 1 to 10.";                           
    }       */


    /*  Noteⁿ: to return a hash code to a specific variable; add{&before variable name}.
     *         Both have the same hash code.
     * 
    */

    /*string l = "Something";
    cout << &l;
    
    string d = "Something else!. ";*/



    //++Calling the functions++
    muFunc();
    sigFunc();
    epsFunc("Ibrahim");

    //ⁿ╚ used with the default argument.
    upsFunc();
    piFunc("Ahmad", "%#^qwerty!@");
    
    //cout << buffer;
    //printf("\nHello Mr.%s Welcome to %s", "Musa","Microsoft");
    //printf("\nHello Mr.%s; Confirm the following %s is your password.", "Muhammad", "$%17&Abstergo");
    
                    // *-----STRING INTEGER CONCATENATION-----*
    int num = 7099;
    //cout << "\nMustafa" + to_string(num);

                    //*-----CLASS IMPLEMENTATIONS-----*
    TestClass test;
    test.name = "\nAhmad Jesus Musa Joseph Jacob";
    cout << "\nClass number attribute = " << test.number;
    test.number = 997401;
    cout << "\nClass number attribute after being changed = " << test.number;
    cout << test.name;

    cout << "\n" << test.innerFuncTest(1, 2);
    cout << "\n" << test.outerFuncTest(1.2, 0.2);
    
    // ⌡■ Parameterised Class Constructor definition.
    TestClass test_2("Amjad");
    TestClass test_3("Omar", "01123144512");
    

    cout << "\nObject 1 = " << test.number;
    cout << "\nObject 2 = " << test_2.number;
    cout << "\nObject 3 = " << test_3.number;

    //test.readFile();
    
    /*
    Exception Handling is approx. like java; 
        __USE {Try -- Catch}... and throw to Specify the Exception [TYPE || CODE]
    */

    try {
        int age = 22;
        if (age < 21){
            cout << "\nAccess can not be granted.";
        }
        else {
            //else do something in catch section....
            throw(age);
        }
       
    }
    catch (int age) {
        printf("\nYou have %s; Access granted Successfully and authorisation has been sent by email.", to_string(age).c_str());
    }
    return 0;
    
}


//╚ Function (B) Definition.
void sigFunc() {
    cout << "\nFunction lower defintion has been successfully accomplished.";
}

//╚ Function (C) Definition.
void epsFunc(string name) {
    cout << "\nAlsalam Aleykum Mr/Mrs.. " << name;
}

//╚ Function (D) Definition.
void upsFunc(int age) {
    char buffer[100];
    sprintf_s(buffer, "\nConfirm that your age is %d; Please be aware that age below 18 is not applicable.", age);
    cout << buffer;
}

void piFunc(string name, string password) {
   //printf("\nAs-salamu Aleykum Mr.%s; Your Password is the following %s; Save it for later usages.", name.c_str(), password.c_str());
    cout << "\nAs-salamu Aleykum Mr." << name << "; Your Password is the following " << password << "; Save it for later usages.";
}

// Correlated Outer Class Method for TestClass.
float TestClass::outerFuncTest(float x, float y){
    return x / y;
}

// Correlated outer constructor definition.
TestClass::TestClass(string name, string phone_num) {
    printf("\nMerhaba %s beyEfendim, Kindly confirm that your phone number is: %s", name.c_str(), phone_num.c_str());
}

// Correlated outer method for reading file.
void TestClass::readFile() {
    string text;

    ifstream MyReadFile("C:\\Users\\omar\\Desktop\\asd.txt");

    while (getline(MyReadFile, text)) {
        cout << text;

    }
    

}